import React, { useState, useEffect, useRef } from 'react';
import { pdfjs } from 'react-pdf';
import EXIF from 'exif-js';
import { forEach, filter } from 'lodash';

// Icons
import { MdChevronRight, MdChevronLeft, MdZoomIn, MdZoomOut } from 'react-icons/md';

import pdfBase64 from '../assets/pdfBase64.js';
import './PdfViewer.css';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const PdfViewer = () => {
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [pages, setPages] = useState(null)
  const [scale, setScale] = useState(1.5);
  const [signatures, setSignatures] = useState([]);

  const canvasEl = useRef(null);

  useEffect( () => {
    loadPdf();
  }, [page, scale]);

  const loadPdf = async () => {
    setLoading(true);
    setSignatures([]);
    const pdf = await pdfjs.getDocument({ data: atob(pdfBase64) }).promise;

    setPages(pdf.numPages);
    const pageDocument = await pdf.getPage(page);

    const viewport = pageDocument.getViewport({ scale });

    const canvas = canvasEl.current;
    const context = canvas.getContext('2d');
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    const renderContext = {
      canvasContext: context,
      viewport: viewport
    };
    await pageDocument.render(renderContext).promise;
    await extractImageCoordinate(pageDocument);

    setLoading(false);
  };

  const extractImageCoordinate = async (pageDocument) => {
    try {
      const options = await pageDocument.getOperatorList()

      const images = {};
      for (let i = 0; i < options.fnArray.length; i++) {
        if (options.fnArray[i] === pdfjs.OPS.paintJpegXObject || options.fnArray[i] === pdfjs.OPS.paintImageXObject) {
          images[options.argsArray[i][0]] = {
            width: options.argsArray[i][1],
            height: options.argsArray[i][2],
            x: options.argsArray[i-2][4],
            y:options.argsArray[i-2][5]
          }
        }
      }

      const signImages = [];
      forEach(images, (value, imageName) => {
        let image = pageDocument.objs.get(imageName);
        EXIF.getData(image, function() {
          const tags = EXIF.getAllTags(this);
          const findSignatures = filter(tags,item => typeof item === 'string' && item.indexOf('signature') > -1);
          if (findSignatures.length) {
            signImages.push(value);
          }
          setSignatures([ ...signatures, ...signImages ]);
        })
      });

      return Promise.resolve(images);
    } catch (error) {
      return Promise.reject(error);
    }
  };

  const onPrevPage = () => {
    const newPage = page - 1;
    if (newPage >= 1) setPage(page - 1);
  };

  const onNextPage = () => {
    const newPage = page + 1;
    if (newPage <= pages) setPage(page + 1);
  };

  // I can also add EventListener to any keyboard button
  const zoomOut = () => {
    const newScale = scale - 0.5;
    setScale(newScale)
  };

  const zoomIn = () => {
    const newScale = scale + 0.5;
    setScale(newScale)
  };

  const renderDivsForSignature = () =>
    signatures.map((signature, index) => {
      let staticMargin = (window.innerWidth - canvasEl.current.width) / 2;
      staticMargin = staticMargin > 0 ? staticMargin : 0;
      const style = {
        width: signature.width * scale * 0.16,
        height: signature.height * scale * 0.16,
        top: signature.y * scale * 0.16,
        left: signature.x * scale + staticMargin
      };
      return (
        <div key={index} style={style} className="pdfViewer__block-for-signature">
          Sign here
        </div>
      );
    });

  return (
    <div className="pdfViewer">
      <div className="pdfViewer__container">
        {loading && (<div className="pdfViewer__loading">Loading page ...</div>)}
        <div id="content" style={{ position: 'relative', display: loading ? 'none' : 'block' }}>
          <canvas ref={canvasEl} />
          {renderDivsForSignature()}
        </div>
      </div>

      <div className="pdfViewer__btns-panel">
        <button disabled={page === 1} onClick={onPrevPage}>
          <MdChevronLeft size={24} />
        </button>

        <button disabled={scale === 0.5} onClick={zoomOut}>
          <MdZoomOut size={24} />
        </button>
        <button disabled={scale === 2} onClick={zoomIn}>
          <MdZoomIn size={24} />
        </button>
        <button disabled={page === pages} onClick={onNextPage}>
          <MdChevronRight size={24} />
        </button>
      </div>
    </div>
  );
}

export default PdfViewer;
