import React from 'react';

import PdfViewer from './components/PdfViewer';

function App() {
  return (
    <div className="App">
      <main>
        <PdfViewer />
      </main>
    </div>
  );
}

export default App;
